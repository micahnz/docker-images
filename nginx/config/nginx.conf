user www-data;
worker_processes auto;
pid /var/run/nginx.pid;

events {
  worker_connections 16384;
  use epoll;
  multi_accept on;
}

http {
  ## MIME types.
  include /etc/nginx/mime.types;
  default_type application/octet-stream;

  ##
  # Basic Settings
  ##
  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  keepalive_timeout 0;
  server_tokens off;

  ## Body size.
  client_max_body_size 64M;

  ##
  # Gzip Settings
  ##
  gzip on;
  gzip_disable "msie6";

  gzip_vary on;
  gzip_proxied any;
  gzip_comp_level 6;
  gzip_buffers 16 8k;
  gzip_http_version 1.1;
  gzip_min_length 256;
  gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript application/vnd.ms-fontobject application/x-font-ttf font/opentype image/svg+xml image/x-icon;

  ## Serve already compressed files directly, bypassing on-the-fly
  # compression.
  gzip_static on;

  ##
  # Logging Settings
  ##
  log_format main '$remote_addr - $remote_user [$time_local] "$request" '
    '$status $body_bytes_sent "$http_referer" '
    '"$http_user_agent" "$http_x_forwarded_for"';

  access_log  /dev/stdout  main;
  error_log /dev/stderr;

  ##
  # Virtual Host Configs
  ##

  include /etc/nginx/conf.d/*.conf;
  include /etc/nginx/sites-enabled/*;
}
