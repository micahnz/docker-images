#!/bin/bash
cp /etc/nginx/conf.d/default.conf.tmpl /etc/nginx/conf.d/default.conf

# defaults
NGINX_SERVER_ROOT=${NGINX_SERVER_ROOT:="/var/www/html/public_html/"}
NGINX_SERVER_PORT=${NGINX_SERVER_PORT:="80"}

# replace variables in config
sed -i "s|\${NGINX_SERVER_ROOT}|$NGINX_SERVER_ROOT|g" /etc/nginx/conf.d/default.conf
sed -i "s|\${NGINX_SERVER_PORT}|$NGINX_SERVER_PORT|g" /etc/nginx/conf.d/default.conf
